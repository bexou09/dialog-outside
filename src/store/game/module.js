const defaultGameParams = {
  level: null
};

export default {
  firestorePath: 'game',
  firestoreRefType: 'collection',
  moduleName: 'game',
  statePropName: 'data',
  namespaced: true,
  state: {
    params: { ...defaultGameParams },
    gameCreated: false,
    started: false,
    players: []
  },
  getters: {
    params: state => {
      return state.params;
    },
    players: state => {
      return state.players;
    },
    roomCreated: state => {
      if (!state.data.roomCreated) {
        return false;
      }
      return state.data.roomCreated.roomCreated;
    },
    level: state => {
      return state.data.level.level;
    },
    started: state => {
      return state.started;
    },
    winner: state => {
      return state.data.winner.winner;
    }
  },
  mutations: {
    setParams: (state, params) => {
      state.params = params;
    },
    addNewPlayer: (state, player) => {
      state.players.push(player);
    },
    removePlayer: (state, playerId) => {
      state.players.splice(
        state.players.indexOf(p => p.id === playerId),
        1
      );
      state.gameCreated = state.players.length !== 0;
    },
    createRoom: state => {
      state.gameCreated = true;
    },
    markPlayerReady: (state, playerId) => {
      const player = state =>
        state.players.find(player => player.id === playerId);
      player.ready = true;
    }
  },
  actions: {
    createNewRoom({ dispatch }, { level }) {
      dispatch('patch', { level: level });
      dispatch('patch', { roomCreated: true });
    },
    setWinner({ dispatch }, playerName) {
      dispatch('patch', { winner: playerName });
    }
  }
};
