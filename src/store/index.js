import Vue from 'vue';
import Vuex from 'vuex';
import VuexEasyFirestore from 'vuex-easy-firestore';
import { Firebase, initFirebase } from '../initFirebase';
import authentication from './authentication/module.js';
import game from './game/module.js';
import player from './player/module.js';
import user from './user/module.js';

Vue.use(Vuex);
// import from step 3 (below)

// do the magic 🧙🏻‍♂️
const easyFirestore = VuexEasyFirestore([game, player], {
  logging: true,
  FirebaseDependency: Firebase
});

// include as PLUGIN in your vuex store
// please note that "myModule" should ONLY be passed via the plugin
const storeData = {
  plugins: [easyFirestore],
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: { authentication, user }
};

// initialise Vuex
const store = new Vuex.Store(storeData);
// initFirebase
initFirebase();

store.dispatch('game/openDBChannel').catch(console.error); // eslint-disable-line no-console
store.dispatch('player/openDBChannel').catch(console.error); // eslint-disable-line no-console

export default store;
