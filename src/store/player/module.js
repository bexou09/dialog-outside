export default {
  firestorePath: 'player',
  firestoreRefType: 'collection',
  moduleName: 'player',
  statePropName: 'data',
  namespaced: true,
  state: {},
  getters: {
    players: state => {
      const data = [];
      for (const x in state.data) {
        data.push(state.data[x]);
      }
      return data;
    }
  },
  mutations: {},
  actions: {
    joinRoom({ dispatch }, player) {
      dispatch('insert', player);
    },
    makePlayerReady({ dispatch }, playerId) {
      dispatch('patch', { id: playerId, ready: true });
    },
    makePlayerUnready({ dispatch }, playerId) {
      dispatch('patch', { id: playerId, ready: false });
    },
    movePlayer({ dispatch }, { playerId, row, column }) {
      dispatch('patch', { id: playerId, row: row, column: column });
    },
    exitRoom({ dispatch }, playerId) {
      dispatch('delete', playerId);
    }
  }
};
