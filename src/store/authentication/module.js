import { clearToken, setToken } from '../../authentication/authTokenTools';
import labyrinthApi from '../../api/labyrinthApi';

const defaultSigninError = {
  email: null,
  password: null,
  message: null
};

const defaultSignupError = {
  email: null,
  confirmPassword: null,
  firstName: null,
  lastName: null,
  team: null,
  password: null,
  message: null
};

export default {
  namespaced: true,
  state: {
    signinIn: false,
    signinError: { ...defaultSigninError },
    signinUp: false,
    signupError: { ...defaultSignupError }
  },
  getters: {
    signingIn: state => {
      return state.signinIn;
    },
    signinUp: state => {
      return state.signinUp;
    },
    signinErrors: state => {
      return state.signinError;
    },
    signupErrors: state => {
      return state.signupError;
    }
  },
  mutations: {
    signinStart: state => {
      state.signinIn = true;
    },
    signinStop: (state, errorMessage) => {
      state.signinIn = false;
      state.signinError = { ...defaultSigninError, ...errorMessage };
    },
    signUpStart: state => {
      state.signinUp = true;
    },
    signUpStop: (state, errorMessage) => {
      state.signinUp = false;
      state.signupError = { ...defaultSignupError, ...errorMessage };
    }
  },
  actions: {
    signIn({ commit, dispatch }, signinData) {
      commit('signinStart');
      const promise = labyrinthApi.signIn({ ...signinData });
      promise
        .then(response => {
          setToken(response.data.token);
          commit('signinStop', null);
          dispatch('user/fetchCurrentUser', {}, { root: true });
        })
        .catch(error => {
          commit('signinStop', error.response.data);
        });
      return promise;
    },
    signUp({ commit, dispatch }, signUpData) {
      commit('signUpStart');
      const promise = labyrinthApi.signUp({ ...signUpData });
      promise
        .then(response => {
          setToken(response.data.token);
          commit('signUpStop', null);
          dispatch('user/fetchCurrentUser', {}, { root: true });
        })
        .catch(error => {
          commit('signUpStop', error.response.data);
        });
      return promise;
    },
    signOut() {
      const promise = new Promise((resolve, _) => {
        clearToken();
        resolve();
      });
      return promise;
    }
  }
};
