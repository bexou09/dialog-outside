import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/MainMenu.vue';
import Lost from '../views/Lost.vue';
import { hasToken } from '../authentication/authTokenTools';
import Signup from '../views/Signup';
import Map from '../views/GameMap/Map';
import WaitingRoom from '../views/WaitingRoom';
import Game from '../views/Game';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/signin',
    name: 'signin',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/Signin.vue')
  },
  {
    path: '/game',
    name: 'game',
    component: Game
  },
  {
    path: '/signup',
    name: 'signup',
    component: Signup,
    meta: {
      requiresAuth: false
    }
  },
  {
    path: '/map',
    name: 'map',
    component: Map,
    meta: {
      requiresAuth: false
    }
  },
  {
    path: '*',
    name: 'lost',
    component: Lost
  },
  {
    path: '/waitingroom',
    name: 'waitingroom',
    component: WaitingRoom
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, _, next) => {
  if (to.matched.some(record => record.meta.requiresAuth) && !hasToken()) {
    next({
      name: 'signin',
      query: { redirect: to.fullPath }
    });
  } else {
    next();
  }
});

export default router;
