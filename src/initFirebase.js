// ~config/firebase.js
import * as Firebase from 'firebase/app';
import 'firebase/firestore';

function initFirebase() {
  Firebase.initializeApp({
    apiKey: 'AIzaSyBk-7MKLQ1AMHzUI8WFq21aIw7LwDH2gHM',
    authDomain: 'dialog-outside.firebaseapp.com',
    databaseURL: 'https://dialog-outside.firebaseio.com',
    projectId: 'dialog-outside',
    storageBucket: 'dialog-outside.appspot.com',
    messagingSenderId: '8908302814',
    appId: '1:8908302814:web:a339376c05675ae6d6d54f'
  });
  return new Promise((resolve, reject) => {
    Firebase.firestore()
      .enablePersistence()
      .then(resolve)
      .catch(err => {
        if (err.code === 'failed-precondition') {
          reject(err);
          // Multiple tabs open, persistence can only be
          // enabled in one tab at a a time.
        } else if (err.code === 'unimplemented') {
          reject(err);
          // The current browser does not support all of
          // the features required to enable persistence
        }
      });
  });
}

export { Firebase, initFirebase };
