import Vue from 'vue';

const urlLabyrinthApi = 'https://labyrinth-api.herokuapp.com/';

const signIn = signinData => {
  return Vue.axios.post(`${urlLabyrinthApi}api/users/signin`, signinData);
};

const signUp = signupData => {
  return Vue.axios.post(`${urlLabyrinthApi}api/users/signup`, signupData);
};

const fetchCurrentUser = () => {
  return Vue.axios.get(`${urlLabyrinthApi}api/users/current`);
};

export const getTileRowsFromApi = level => {
  const url = `${urlLabyrinthApi}maze/${level}`;
  return Vue.axios
    .get(url)
    .then(response => response.data)
    .then(tileRows => {
      const mapTileRows = [];
      let id = 1;
      let tileWidth = 130;

      if (level === 'advanced') {
        tileWidth = 45;
      }

      for (let i = 0; i < tileRows.length; i += 1) {
        const currentTileRow = tileRows[i];
        const tiles = [];

        for (let j = 0; j < currentTileRow.length; j += 1) {
          const currentTile = currentTileRow[j];
          tiles.push({
            id: id,
            width: tileWidth,
            canCrossTop: currentTile.top !== 1,
            canCrossRight: currentTile.right !== 1,
            canCrossLeft: currentTile.left !== 1,
            canCrossBottom: currentTile.bottom !== 1
          });
          id += 1;
        }
        const tileRow = {
          id: id,
          tiles: tiles,
          width: tileWidth / 2
        };
        id += 1;
        mapTileRows.push(tileRow);
      }
      return mapTileRows;
    });
};

export default { signIn, signUp, fetchCurrentUser, getTileRowsFromApi };
